========================
yin2009
========================

.. {# pkglts, doc

.. image:: https://b326.gitlab.io/yin2009/_images/badge_pkging_pip.svg
    :alt: PyPI version
    :target: https://pypi.org/project/yin2009/0.1.0/

.. image:: https://b326.gitlab.io/yin2009/_images/badge_pkging_conda.svg
    :alt: Conda version
    :target: https://anaconda.org/revesansparole/yin2009

.. image:: https://b326.gitlab.io/yin2009/_images/badge_doc.svg
    :alt: Documentation status
    :target: https://b326.gitlab.io/yin2009/

.. image:: https://badge.fury.io/py/yin2009.svg
    :alt: PyPI version
    :target: https://badge.fury.io/py/yin2009

.. #}
.. {# pkglts, glabpkg_dev, after doc

main: |main_build|_ |main_coverage|_

.. |main_build| image:: https://gitlab.com/b326/yin2009/badges/main/pipeline.svg
.. _main_build: https://gitlab.com/b326/yin2009/commits/main

.. |main_coverage| image:: https://gitlab.com/b326/yin2009/badges/main/coverage.svg
.. _main_coverage: https://gitlab.com/b326/yin2009/commits/main
.. #}

C3 and C4 photosynthesis models

