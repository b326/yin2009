"""
Jmax temperature dependence
===========================
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from yin2009 import pth_clean, raw

# params
table2 = pd.read_csv(pth_clean / "table2.csv", sep=";", comment="#", index_col=['constant'])

tab2 = table2['value'].to_dict()

records = []
for t_leaf in np.linspace(0, 50, 100):
    records.append(dict(
        t_leaf=t_leaf,
        j_max_temp=raw.eq6(t_leaf, 1., tab2['jmax_act'], tab2['jmax_deact'], tab2['jmax_s']),
        j_max_temp_min=raw.eq6(t_leaf, 1., tab2['jmax_act_min'], tab2['jmax_deact'], tab2['jmax_s']),
        j_max_temp_max=raw.eq6(t_leaf, 1., tab2['jmax_act_max'], tab2['jmax_deact'], tab2['jmax_s'])
    ))

df = pd.DataFrame(records)

# plot shape of curve
fig, axes = plt.subplots(1, 1, figsize=(10, 5), squeeze=False)
ax = axes[0, 0]
ax.plot(df['t_leaf'], df['j_max_temp'], label="jmax_act avg")
ax.plot(df['t_leaf'], df['j_max_temp_min'], '--', label="jmax_act min")
ax.plot(df['t_leaf'], df['j_max_temp_max'], '--', label="jmax_act max")

ax.legend(loc='upper left')
ax.set_xlabel("t_leaf [°C]")
ax.set_ylabel("jmax temperature dependency [-]")

fig.tight_layout()
plt.show()
