"""
Fig2
====

Evolution of An for different levels of radiation and cc
"""
import matplotlib.pyplot as plt
import pandas as pd

from yin2009 import pth_clean

# read data
df = pd.read_csv(pth_clean / "fig2.csv", sep=";", comment="#")

# plot data
fig, axes = plt.subplots(2, 1, sharex='all', figsize=(8, 8), squeeze=False)
ax = axes[0, 0]

for i_abs, sdf in df[df['cc'] == 245].groupby('i_abs'):
    ax.plot(sdf['t_leaf'], sdf['an'], label=f"{i_abs:d}")

ax.legend(loc='upper left')
ax.set_ylim(0, 25)
ax.set_ylabel("CO2 fixation rate [µmol CO2.m-2.s-1]")

ax = axes[1, 0]
for cc, sdf in df[df['i_abs'] == 500].groupby('cc'):
    ax.plot(sdf['t_leaf'], sdf['an'], label=f"{cc:d}")

ax.legend(loc='upper left')
ax.set_ylim(0, 30)
ax.set_xlabel("Leaf temperature [°C]")
ax.set_ylabel("CO2 fixation rate [µmol CO2.m-2.s-1]")

fig.tight_layout()
plt.show()
